"""bubblegame URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from users.views import RegistrationView, LoginView, Logoutview, HomeView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('user/', include('users.urls')),
    path('user/me/shop/', include('items.urls')),
    path('signup/', RegistrationView.as_view(), name='signup'),
    path('login/', LoginView.as_view(), name='login'),
    path('', HomeView.as_view(template_name="users/home.html"), name='home'),
    path('logout/', Logoutview.as_view(), name="logout"),




    # path('user_messages/', views.ContactList.as_view(), name='message_table_list'),
    # path('contact/<int:pk>', views.ContactDetail.as_view(), name='message_table_detail'),
    # path('create', views.ContactCreate.as_view(), name='message_table_create'),
    # path('update/<int:pk>', views.ContactUpdate.as_view(), name='message_table_update'),
    # path('delete/<int:pk>', views.ContactDelete.as_view(), name='message_table_delete'),
]




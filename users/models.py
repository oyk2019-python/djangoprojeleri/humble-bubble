from django.db import models
from django.contrib.auth.models import AbstractUser


class UserTable(AbstractUser):

    user_name=models.CharField(max_length=32, verbose_name="user name", unique=True)
    user_password=models.CharField(max_length=32, verbose_name="password")
    char_level=models.IntegerField(default=1)
    no_of_item=models.IntegerField(default=0)
    gold=models.IntegerField(default=200)
    is_working=models.BooleanField(default=False)




# Generated by Django 2.2.3 on 2019-07-24 07:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='usertable',
            name='is_working',
            field=models.BooleanField(default=False),
        ),
    ]
from django.views.generic import FormView, DetailView
from users.forms import UserRegisterForm
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView
from django.contrib.auth.views import LogoutView
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import UserTable
from django.contrib.auth.forms import AuthenticationForm
from django.http import request
from django.contrib.auth import login as auth_login, logout as auth_logout




class RegistrationView(FormView):
    form_class = UserRegisterForm
    template_name = 'users/signup.html'
    success_url = reverse_lazy("login")

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)



class LoginView(FormView):
    """
    Provides the ability to login as a user with a username and password
    """
    success_url = reverse_lazy("me")
    form_class = AuthenticationForm
    template_name = 'users/login.html'

    def form_valid(self, form):
        auth_login(self.request, form.get_user())
        # If the test cookie worked, go ahead and
        # delete it since its no longer needed
        if self.request.session.test_cookie_worked():
            self.request.session.delete_test_cookie()
        return super(LoginView, self).form_valid(form)


# class LogoutView(RedirectView):
#     """
#     Provides users the ability to logout
#     """
#     url = '/users/login/'
#
#     def get(self, request, *args, **kwargs):
#         auth_logout(request)
#         return super(LogoutView, self).get(request, *args, **kwargs)

class Logoutview(LogoutView):
    next_page = '/login'


# class UserDetailView(DetailView, LoginRequiredMixin):
#     template_name = 'users/home.html'
#     context_object_name = 'user_detail'
#     model = UserTable
#     is_me = False
#
#     def get_object(self, queryset=None):
#         if self.is_me:
#             return self.request.user
#         else:
#             print(UserTable.objects.filter(pk=request.owner.id)
#             # return super().get_object(queryset)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context["is_me"] = self.is_me
#         return context


class HomeView(TemplateView):
    pass
from django.contrib import admin
from .models import ItemType, AddItemToUser

# Register your models here.

admin.site.register(AddItemToUser)
admin.site.register(ItemType)


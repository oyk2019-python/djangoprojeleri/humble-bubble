from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView, View, CreateView, FormView, UpdateView
from django.db.models import F
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse,Http404, request, HttpResponseRedirect
from django.urls import reverse_lazy
from .models import AddItemToUser, ItemType
from users.models import UserTable




# Create your views here.


class ItemsListView(LoginRequiredMixin,ListView):

    model = ItemType
    template_name = "items/shop.html"
    queryset = ItemType.objects.all()



class ItemsDetailView(DetailView):
    model = ItemType
    template_name = "items/item_detail.html"



class ItemBuyView(UpdateView):
    template_name = "items/success.html"
    model = UserTable

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            UserTable.objects.filter(pk=request.user.id).update(no_of_item = F('no_of_item')+1)
            return render(request, 'items/shop.html')
        else:
            return render(request, 'users/home')


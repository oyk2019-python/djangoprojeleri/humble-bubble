from django.urls import path

from .views import ItemsListView, ItemBuyView

urlpatterns = [
    path('all_items/', ItemsListView.as_view(), name='shop'),
    path('all_items/success/', ItemBuyView.as_view(), name='bought'),
]

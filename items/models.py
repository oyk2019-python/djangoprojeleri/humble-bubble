from django.db import models
from django.db.models import F
from django.conf import settings
from users.models import UserTable

# Create your models here.


class ItemType(models.Model):

    TYPES = (
        (1, "Face-Guard"),
        (2, "Body-Armor"),
        (3, "Leg-Armor"),
        (4, "Weapon"),
        (5, "Boost-Item"),
    )
    name=models.CharField(max_length=30)
    type=models.IntegerField(choices=TYPES)
    attack=models.IntegerField()
    defence=models.IntegerField()
    description=models.CharField(max_length=150, verbose_name="Info", blank=True)

    def __str__(self):
        return self.name

class AddItemToUser(models.Model):

    owner=models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="owner")
    item=models.ForeignKey(ItemType, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Item To User"
        verbose_name_plural = "Items on users"


    def save(self, *args, **kwargs):
        UserTable.objects.filter(pk=self.owner_id).update(no_of_item=F('no_of_item')+1)
        return super().save(*args, **kwargs)
        # obj = UserTable.objects.get(pk=self.owner.id)
        # obj.no_of_item +=1
        # obj.save()
        # return super(AddItemToUser,self).save()

    def __str__(self):
        return "%s - %s" %(self.owner,self.item)

# Generated by Django 2.2.3 on 2019-07-27 13:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('items', '0006_auto_20190727_1249'),
    ]

    operations = [
        migrations.RenameField(
            model_name='additemtouser',
            old_name='itemtype',
            new_name='item',
        ),
    ]
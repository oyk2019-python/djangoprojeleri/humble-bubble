# Generated by Django 2.2.3 on 2019-07-29 07:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('items', '0010_auto_20190728_1617'),
    ]

    operations = [
        migrations.AddField(
            model_name='itemtype',
            name='description',
            field=models.CharField(blank=True, max_length=150, verbose_name='Info'),
        ),
    ]
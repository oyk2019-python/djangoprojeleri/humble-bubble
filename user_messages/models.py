from django.db import models
from django.conf import settings

# Create your models here.


class MessageTable(models.Model):
    sender = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, related_name='messages_from_me')
    receiver = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, related_name='messages_to_me')
    message_text = models.TextField(max_length=140)
    created = models.DateTimeField(auto_now_add=True)
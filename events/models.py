from django.db import models
from django.conf import settings

# Create your models here.


class EventTable(models.Model):
    attacker = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING, related_name='attacker')
    defencer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING, related_name='defencer')
    winner = models.IntegerField(default=0)
    prize = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)
    description = models.TextField(max_length=300)







